
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.237');


# ---------------------------------------------------------------------- #
# Modify table contact_history_action                                    #
# ---------------------------------------------------------------------- #
ALTER TABLE `contact_history_action`
	ADD COLUMN `Private` ENUM('Y','N') NULL DEFAULT 'N';


 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.238');
