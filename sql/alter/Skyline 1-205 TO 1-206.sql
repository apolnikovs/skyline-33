# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.205');

# ---------------------------------------------------------------------- #
# Modify table "audit_new"                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE audit_new ALTER Action DROP DEFAULT; ALTER TABLE audit_new CHANGE COLUMN Action Action ENUM('update','delete','insert') NOT NULL AFTER TableName;




# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.206');
