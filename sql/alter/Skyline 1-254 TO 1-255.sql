# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.254');

# ---------------------------------------------------------------------- #
# Modify Table service_provider                                          #
# ---------------------------------------------------------------------- # 
ALTER TABLE service_provider ADD COLUMN ServiceProviderType ENUM('Standard','Branch') NOT NULL DEFAULT 'Standard' AFTER LockAppWindowTime;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.255');
