# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.234');


# ---------------------------------------------------------------------- #
# Modify table completion_status                                         #
# ---------------------------------------------------------------------- #
ALTER TABLE `completion_status`
 CHANGE COLUMN `Description` `Description` TEXT NULL DEFAULT NULL AFTER `CompletionStatus`;

	
	
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.235');
