<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of County Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class County extends CustomModel {
    
    private $conn;
    private $dbColumns = array('t1.CountyID', 't1.CountyCode', 't1.Name',  't3.Name AS Cname', 't1.Status', 't2.BrandName', 't1.BrandID');
    private $tables    = "county AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID LEFT JOIN country AS t3 ON t3.CountryID=t1.CountryID AND t1.BrandID=t3.BrandID";
    private $table     = "county";
    private $SkylineBrandID = 1000;
    
      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
       
        /*
        
        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')  || 
                (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '' )) {
           
            $args['where'] = '(';
            
            if ( $args['StockCodeSearchStr'] != '') {
                
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
                }
                
            }
            else
            {
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                }
            }
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        */
        //done by nivas on 20-06-2013
        //asssigned this varible for fetching PDO result in associate format
        $args['assoc'] = 1;
        
        if($this->controller->user->SuperAdmin)
        {
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        }
        else if(is_array($this->controller->user->Brands))
        {    
            
           $brandsList  = implode(",", array_keys($this->controller->user->Brands));
           
           
           
           if($brandsList)
           {
                $brandsList .= ",".$this->controller->SkylineBrandID;
           }    
          
          
            $args['where'] = "t1.BrandID IN (".$brandsList.")";
            
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
       
        }
        
        
        
        
        
        
       
        
        return  $output;
        
    }
    
    
    /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['CountyID']) || !$args['CountyID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
     
     /**
     * Description
     * 
     * This method finds the maximum code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
     * @global $this->table
     * @return integer It returns maximum code if it finds in the database table otherwise it returns 0.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */  
     public function getCode($BrandID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT CountyCode FROM '.$this->table.' WHERE BrandID=:BrandID ORDER BY CountyID DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        //$this->controller->log(var_export($BrandID, true));
        
        $fetchQuery->execute(array(':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return 0;
        }
       
    }
    
    
     /**
     * Description
     * 
     * This method is used for to validate action code for given brand.
     *
     * @param interger $CountyCode  
     * @param interger $BrandID.
     * @param interger $CountyID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($CountyCode, $BrandID, $CountyID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT CountyID FROM '.$this->table.' WHERE CountyCode=:CountyCode AND BrandID=:BrandID AND CountyID!=:CountyID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':CountyCode' => $CountyCode, ':BrandID' => $BrandID, ':CountyID' => $CountyID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['CountyID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
//        $sql = 'INSERT INTO '.$this->table.' (Name, CountyCode, CountryID, Status, BrandID)
//            VALUES(:Name, :CountyCode, :CountryID, :Status, :BrandID)';
        
        $fields = explode(', ', 'Name, CountyCode, CountryID, Status, BrandID');
        $fields = array_combine($fields , $fields);
        
        $sql = TableFactory::County()->insertCommand($fields);        
        
        if(!isset($args['CountyCode']) || !$args['CountyCode'])
        {
            $args['CountyCode'] = $this->getCode($args['BrandID'])+1;//Preparing next name code.
        }
       // $this->controller->log(var_export('tessss', true));
        
        if($this->isValidAction($args['CountyCode'], $args['BrandID'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(':Name' => $args['Name'], ':CountyCode' => $args['CountyCode'], ':CountryID' => $args['CountryID'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID']));
        
        
              return array('status' => 'OK',
                        'message' => $this->controller->page['data_inserted_msg']);
        }
         else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT CountyID, CountyCode, Name, CountryID, Status, BrandID FROM '.$this->table.' WHERE CountyID=:CountyID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':CountyID' => $args['CountyID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch county id for given BrandID and post code area.
     *
     * @param int $BrandID
     * @param string $PostcodeArea
     * @global $this->table  
     * @return array It contains County and Country IDs.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function getCountyIDFromPostCode($BrandID, $PostcodeArea) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT T1.CountyID, T1.Name, T1.CountryID FROM '.$this->table.' AS T1 LEFT JOIN postcode AS T2 ON T1.CountyCode=T2.CountyCode WHERE T1.Status=:Status AND (T1.BrandID=:BrandID OR T1.BrandID=:SkylineBrandID) AND T2.Postcode=:Postcode ORDER BY T1.BrandID DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':BrandID' => $BrandID, ':SkylineBrandID' => $this->SkylineBrandID, ':Postcode' => $PostcodeArea, ':Status' => 'Active'));
        
        $result = $fetchQuery->fetch();
        
        if(is_array($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
        
    }
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['CountyCode'], $args['BrandID'], $args['CountyID']))
        {        
            $fields = explode(', ', 'Name, CountyCode, CountryID, Status, BrandID');
            $fields = array_combine($fields , $fields);
            
            $where = 'CountyID=:CountyID';
            
            $sql = TableFactory::County()->updateCommand($fields, $where);   
            
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET Name=:Name, CountyCode=:CountyCode, CountryID=:CountryID, Status=:Status, BrandID=:BrandID
            WHERE CountyID=:CountyID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(array(':Name' => $args['Name'], ':CountyCode' => $args['CountyCode'], ':CountryID' => $args['CountryID'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID'], ':CountyID' => $args['CountyID']));
        
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    
    
}
?>