<?php

require_once('CustomModel.class.php');

class Emsoap extends CustomModel  {
    
    private $client;
    private $error;
    
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->error = '';

        $uri = "";
       try {
        $this->client =
            new SoapClient($this->controller->config['SMS']['URL'] . '?WSDL',
                            array('Account' =>  $this->controller->config['SMS']['Account'], 'Password' => $this->controller->config['SMS']['Password']));
        } catch (Exception $e) {
            $this->controller->log(var_export($e->getMessage(), true));
            return false;
        }
    }
    
    public function sendSMS($args) {
        
        if(!$this->client)
        {
            return false;
        }
        $this->error = '';
        $params = new stdClass();

        try {
            
            $params->Account = $this->controller->config['SMS']['Account'];
            $params->Password = $this->controller->config['SMS']['Password'];
            $params->Subject = $args['Subject'];
            $params->Body = $args['Body'];
            $params->MobileCSV = $args['Mobile'];
            $params->DeliveryReceipt = 1;
            $params->HTTPOnly = 1;
            $params->ReplyEmail = '';
            $params->Scheduled = '';
            $params->Originator = '';
            
            
            
            
          //  $response = $this->client->SendBasicMessage($params);
            
            //$this->log('tesssssssssssssss');
            //$this->log($response);
            
            if(isset($response->SendBasicMessageResult) && $response->SendBasicMessageResult=='SUCCESS')
            {
                return true;
            } 
            else 
            {
                return false;
            }
                     
            
        } catch (Exception $e) {
            $this->error =  $e->getMessage();
            $this->LogError(__METHOD__, $params, $this->error);
            return false;
        }
    }
    
    public function Error() {
        return $this->error;
    }
        
    private function LogError($func, $args, $error) {
        
        $s = $func;
        foreach($args as $key => $value) {
            $s .= "\n$key = $value";
        }
        $s .= "\nError: $error";
        $this->controller->log($s);
    }
    
    
     
    
}

?>
