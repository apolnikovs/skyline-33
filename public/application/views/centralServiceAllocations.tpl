{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $CentralServiceAllocations}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
    .ui-combobox-input {
        width:300px;
    }
    </style>
{/block}

{block name=scripts}


    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
                 
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    /**
    *  This is used to change color of the row if its in-active.
    */
    function inactiveRow(nRow, aData)
    {
          
        if (aData[10]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(9)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(9)', nRow).html( $statuses[0][0] );
        }
        
    }
    
    
    function gotoInsertPage($sRow)
    {
       
        $("#copyRowId").val($sRow[0]);
        $('#addButtonId').trigger('click');
        $("#copyRowId").val('');
        
        
    }
    

    $(document).ready(function() {



                  //Click handler for finish button.
                    $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/JobAllocation');

                                });

                                      



                      
$("#nId").combobox({
    change: function() {
        $(".dataTables_wrapper").remove();
        var the = this;
        if($(this).val() != "") {
            $(location).attr('href', '{$_subdomain}/JobAllocation/centralServiceAllocations/'+urlencode($("#nId").val())); 
        }
    }
});
$("#mId").combobox({
    change: function() {
        $(".dataTables_wrapper").remove();
        var the = this;
        if($(this).val() != "") {
            $(location).attr('href', '{$_subdomain}/JobAllocation/centralServiceAllocations/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())); 
        }
    }
});
                     /* Add a change handler to the network dropdown - strats here*/
                        /*$(document).on('change', '#nId', 
                            function() {
                                
                            
                                $(location).attr('href', '{$_subdomain}/JobAllocation/centralServiceAllocations/'+urlencode($("#nId").val())); 
                            }      
                        );*/
                      /* Add a change handler to the network dropdown - ends here*/
                      
                    /* Add a change handler to the manufactrer dropdown - strats here*/
                        /*$(document).on('change', '#mId', 
                            function() {
                                
                                $(location).attr('href', '{$_subdomain}/JobAllocation/centralServiceAllocations/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())); 
                            }      
                        );*/
                       /* Add a change handler to the manufactrer dropdown - ends here*/
                     
              

                    /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                    

                      var  displayButtons = "UPA";
                     
                     
                         
                         
                         
                       
                           
                    $columnsList    = [ 
                                                /* CentralServiceAllocationID */  {  "bVisible":    false },    
                                                /* Service Centre */   null,
                                                /* Country */   null,
                                                /* Client */   null,
                                                /* Manufacturer */   null,
                                                /* Repair Skill */   null,
                                                /* Job type */   null,
                                                /* Service Type */   null,
                                                /* Unit Type */   null,
                                                /* County */   null,
                                                /* Status */  null
                                                
                                        ];
                          
                         
                       
     
                     
                   
                    
                    $('#CentralServiceAllocationsResults').PCCSDataTable( {
                              "aoColumns": $columnsList,
                               
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/JobAllocation/centralServiceAllocations/insert/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())+"/",
                            createDataUrl:   '{$_subdomain}/JobAllocation/ProcessData/CentralServiceAllocations/',
                            createFormFocusElementId:'NetworkID',
                            formInsertButton:'insert_save_btn',
                            copyInsertRowId: 'copyRowId',
                            
                            
                            frmErrorRules:   {
                                                    
                                                    NetworkID:
                                                        {
                                                            required: true
                                                        },
                                                    CountryID:
                                                        {
                                                            required: true
                                                        },
                                                    RepairSkillID:
                                                        {
                                                            required: true
                                                        },
                                                    ClientID:
                                                        {
                                                            required: function (element) {
                                                                if($("#ClientID").val()=='' && $("#ManufacturerID").val()=='')
                                                                {

                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    return false;
                                                                }
                                                            }
                                                        }      
                                                        
                                                        
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    NetworkID:
                                                        {
                                                            required: "{$page['Errors']['network']|escape:'html'}"
                                                        },
                                                    CountryID:
                                                        {
                                                             required: "{$page['Errors']['country']|escape:'html'}"
                                                        },
                                                    RepairSkillID:
                                                        {
                                                             required: "{$page['Errors']['repair_skill']|escape:'html'}"
                                                        },
                                                    ClientID:
                                                        {
                                                            required: "{$page['Errors']['client_manufacturer']|escape:'html'}"
                                                        }     
                                                     
                                              },                     
                            
                            popUpFormWidth:  750,
                            popUpFormHeight: 430,
                            
                            pickButtonId:"copy_btn",
                            pickButtonText:"{$page['Buttons']['copy']|escape:'html'}",
                            pickCallbackMethod: "gotoInsertPage",
                            colorboxForceClose: false,
                           
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/JobAllocation/centralServiceAllocations/update/',
                            updateDataUrl:   '{$_subdomain}/JobAllocation/ProcessData/CentralServiceAllocations/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId:'NetworkID',
                            
                            colorboxFormId:  "CentralServiceAllocationsForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'CentralServiceAllocationsResultsPanel',
                            htmlTableId:     'CentralServiceAllocationsResults',
                            fetchDataUrl:    '{$_subdomain}/JobAllocation/ProcessData/CentralServiceAllocations/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$mId}"),
                            formCancelButton:'cancel_btn',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:'dataTables_command'


                        });
                      

                        
                   

    });

    </script>

{/block}


{block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/jobAllocation" >{$page['Text']['job_allocation']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="CentralServiceAllocationsTopForm" name="CentralServiceAllocationsTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="CentralServiceAllocationsResultsPanel" >
                    <form id="nIdForm" class="nidCorrections">
                        {if $SupderAdmin eq true} 
                            {$page['Labels']['network_label']|escape:'html'}
                            <select name="nId" id="nId" >
                                <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                            &nbsp;&nbsp;  

                        {else}

                            <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >

                        {/if} 
                        <br /><span style="padding-right: 15px;">{$page['Labels']['manufacturer_label']|escape:'html'}</span>
                        <select name="mId" id="mId" >
                            <option value="" {if $mId eq ''}selected="selected"{/if}>{$page['Text']['select_manufacturer']|escape:'html'}</option>

                            {foreach $manufacturers as $manufacturer}

                                <option value="{$manufacturer.ManufacturerID}" {if $mId eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                            {/foreach}
                        </select>
                     </form>
                    
                    <form id="CentralServiceAllocationsResultsForm" class="dataTableCorrections">
                        <table id="CentralServiceAllocationsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            <th></th> 
                                            <th title="{$page['Text']['service_centre']|escape:'html'}" >{$page['Text']['service_centre']|escape:'html'}</th>
                                            <th title="{$page['Text']['country']|escape:'html'}" >{$page['Text']['country']|escape:'html'}</th>
                                            <th title="{$page['Text']['client']|escape:'html'}" >{$page['Text']['client']|escape:'html'}</th>
                                            <th title="{$page['Text']['manufacturer']|escape:'html'}" >{$page['Text']['manufacturer']|escape:'html'}</th>
                                            <th title="{$page['Text']['repair_skill']|escape:'html'}" >{$page['Text']['repair_skill']|escape:'html'}</th>
                                            <th title="{$page['Text']['job_type']|escape:'html'}" >{$page['Text']['job_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['service_type']|escape:'html'}" >{$page['Text']['service_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['unit_type']|escape:'html'}" >{$page['Text']['unit_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['county']|escape:'html'}" >{$page['Text']['county']|escape:'html'}</th>
                                            <th title="{$page['Text']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>
                                    </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                        
                     </form>
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                    
                </div>        
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               <input type="hidden" name="copyRowId" id="copyRowId" > 
                 
    </div>
                        

{/block}



