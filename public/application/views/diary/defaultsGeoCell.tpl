{extends "DemoLayout.tpl"}

{block name=config}

{$PageId = $SkillsAreaMapPage}
{$def=0}
{if $SuperAdmin}{$def=1}{/if}
 {if !isset($allocOnly)||$allocOnly!='AllocationOnly'}
     {$def=1}
     {/if}
    {if $allocOnly=='NoViamente'}
     {$def=2}
     {/if}
{/block}

{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
   <style type="text/css" >
      
    .ui-combobox-input {
        width:270px;
     }   
    .ui-tabs-selected a { font-weight: bold; }     
    
    .ui-datepicker-trigger { float: left; margin-top:7px; }
    
    .ui-state-active {
    
    }
    
</style>   
{/block}

{block name=scripts}

{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 


{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}

    




<script type="text/javascript">
    
   function inactiveRow(nRow, aData)
    {
        //$('td:eq(0)', nRow).attr("id", aData[0]);
        
        
        if(aData[0]=="{$datarow.ServiceProviderEngineerID}")
        {
            $checked=" checked='checked' ";
        }
        else
        {
            $checked="";
        }
        
        $('td:eq(0)', nRow).parent().attr("id", "EngID_"+aData[0]);
        
        $('td:eq(0)', nRow).html( '<input type="radio" name="ServiceProviderEngineerID" value="'+aData[0]+'" '+$checked+' >'+aData[1]);

        if (aData[2]=="In-active")
        {  
             $('td:eq(1)', nRow).html( '<img src="{$_subdomain}/css/Skins/{$_theme}/images/red_cross.png" align="center"  width="20" height="20" >' );
        }
        else
        {   
              $('td:eq(1)', nRow).html( '<img src="{$_subdomain}/css/Skins/{$_theme}/images/green_tick.png" align="center" width="20" height="20" >' );
        }
    }
    
    
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }
    
    
    function setEngineerValue()
    {
            $("#ServiceProviderEngineerIDHidden").val('');
            
            
            
                                 
            if ($("input[name='ServiceProviderEngineerID']:checked").val()) {

              $("#ServiceProviderEngineerIDHidden").val($("input[name='ServiceProviderEngineerID']:checked").val());

              // alert("Hi");
            }
            
            if(!$("input[name='ServiceProviderEngineerID']").length)
            {
                {if $datarow.ServiceProviderEngineerID neq ''}

                    $("#ServiceProviderEngineerIDHidden").val("{$datarow.ServiceProviderEngineerID}");

                {/if}
            }
            
            $("#AllocatedDateHidden").val($("#AllocatedDate").val());
            $("#AllocatedToDateHidden").val($("#AllocatedToDate").val());
        
    }  
    
    $(document).ready(function() {
	
	 $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
    
     $(".TimeSlotActiveTab").removeClass("TimeSlotActiveTab");
     $("#Tab"+$("#AppointmentAllocationSlotID").val()+" a").addClass("TimeSlotActiveTab");
   
   
   
   
     function adjustPanelHeights ()
     {
        $('#firstDiv').css("height", '');
        $('#secondDiv').css("height", ''); 
        $('#innerFirstDiv').css("height", '');
         
        $height1 = $('#firstDiv').height();
        $height2 = $('#secondDiv').height()+10;

        $maxHeight = 300;
        
        if($height1>$maxHeight)
        {
           $maxHeight = $height1;
        } 
        
        if($height2>$maxHeight)
        {
           $maxHeight = $height2;
        } 

        $maxHeight = $maxHeight+40;
        
        $('#firstDiv').css("height", $maxHeight+"px");
        $('#secondDiv').css("height", $maxHeight+"px");
        $('#innerFirstDiv').css("height", ($maxHeight-30)+"px");
     }
     
     
    
       adjustPanelHeights();
     
     
                      
        //Click handler for cancelChanges.
        $(document).on('click', '#cancelChanges', 
                      function() {
                      
                       // document.location.href = "{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val();
                       // return false;
                       
                       
                       if (confirm("{$page['Text']['cancel_confirm']|escape:'html'}")) {
                       
                           document.location.href = "{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val();
                           
                       }
                       
                       return false;
                       
                          
                      }); 
     
     
     //Form click handler.
     $(document).on('click', '#skillsAreaMapForm', 
                      function() {
                        
                        formElemnts();
                      
                      });
     //Form focus handler.                 
     $(document).on('focus', '#skillsAreaMapForm', 
                      function() {
                        
                        formElemnts();
                      
                      });                                  
      
                      
     //Click handler for Replicate Weeks checkbox.
     $(document).on('click', '#ReplicateWeeks', 
                      function() {
                      
                      
                        if ($('#ReplicateWeeks').is(':checked')) 
                        {
                            $("#WeekNavigationElement").hide();
                        } 
                        else 
                        {
                            if($("#TotalWeeksValue").val()!="1" && $("#TotalWeeksValue").val()!='')
                            {    
                                $("#WeekNavigationElement").show();
                            }
                            else
                            {
                                $("#WeekNavigationElement").hide();
                            }
                        } 
                        
                        $('#secondDiv').css("height", '');
                        adjustPanelHeights();
                      
                      });
    
    
                      
    
     //Skill set radio buttons
     $(document).on('click', 'input[name=ServiceProviderEngineerID]:radio', 
                      function() {
                        
                        setEngineerValue();
                
                        //To do: get the data from selected week. 
                        fillFormData($("#ServiceProviderEngineerIDHidden").val(), $("#AllocatedDate").val(), $("#AllocatedToDate").val(),  $("#AppointmentAllocationSlotID").val(), $("#CurrentWeekNumber").val());

                        formElemnts();
                      
                      });                 
        
          
     function formElemnts()
     {
         setEngineerValue();
         
         $readOnly = true;
         
         if($("#ServiceProviderEngineerIDHidden").val()!='' && $("#AllocatedDate").val()!='' && $("#AppointmentAllocationSlotID").val()!='')
         {
             $readOnly = false;
         }    
         
         if($readOnly)
         {    
            for($i=1;$i<=7;$i++)
            {    
                $("#AllocatedSlots_"+$i).attr("readonly", "readonly");
                $("#Postcode_"+$i).attr("readonly", "readonly");
            }
            
            $("#centerInfoText").html("{$page['Errors']['fill_skillset_date']|escape:'html'}").css('color','red').fadeIn('slow'); 
         }
         else
         {
            for($i=1;$i<=7;$i++)
            {    
                $("#AllocatedSlots_"+$i).removeAttr("readonly");
                $("#Postcode_"+$i).removeAttr("readonly");
            }
            
            $("#centerInfoText").html(""); 
         }
     }   
           
    $( "#tabs" ).tabs({
        select: function(event, ui) { 
    
                 $value = ui.panel.id.replace("tabs-", "");
                 $('#selectedTabSlot').val($value);
                 if($value)
                 {
                        $("#AppointmentAllocationSlotID").val($value);
                        
                        
                        $(".TimeSlotActiveTab").removeClass("TimeSlotActiveTab");
                        
                        $("#Tab"+$("#AppointmentAllocationSlotID").val()+" a").addClass("TimeSlotActiveTab");
                        
                        
                        setEngineerValue();
                
                        //To do: get the data from selected week. 
                        fillFormData($("#ServiceProviderEngineerIDHidden").val(), $("#AllocatedDate").val(), $("#AllocatedToDate").val(), $("#AppointmentAllocationSlotID").val(), $("#CurrentWeekNumber").val());

                        formElemnts();
                        
                        
                 }
                 else 
                 {
                        $("#AppointmentAllocationSlotID").val(0);
                       
                        $(".TimeSlotActiveTab").removeClass("TimeSlotActiveTab");
                        
                        $("#Tab"+$("#AppointmentAllocationSlotID").val()+" a").addClass("TimeSlotActiveTab");
                       
                 }
          }
     });
    
     {if $SuperAdmin}
     
        $( "#ServiceProviderID" ).combobox( { change: function() { document.location.href="{$_subdomain}/AppointmentDiary/skillsAreaMap/spID="+$("#ServiceProviderID").val(); } } );
    
     {/if}
 
 
 
     $( "#AllocatedDate" ).datepicker({
     
                dateFormat: "dd/mm/yy",
                showOn: "button",
                minDate: '-6d',
                showWeek: true,
                buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
                buttonImageOnly: true,
                beforeShowDay:  function(dt)
        	      {
        	        return [dt.getDay() == 1, ""];
        	      },
                      
                onSelect: function (dateText, inst) {
                  
                  $('#AllocatedToDate').datepicker("option", 'minDate', dateText);
                  
                },       
    
                onClose: function(dateText, inst) { 
                
                 setEngineerValue();
                
                 //To do: get the data from selected week. 
                 fillFormData($("#ServiceProviderEngineerIDHidden").val(), $("#AllocatedDate").val(), $("#AllocatedToDate").val(), $("#AppointmentAllocationSlotID").val(), $("#CurrentWeekNumber").val());
                 
                 formElemnts();
                    
                }
            });
            
            
            
     $( "#AllocatedToDate" ).datepicker({
     
                dateFormat: "dd/mm/yy",
                showOn: "button",
                minDate: '+0d',
                showWeek: true,
                buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
                buttonImageOnly: true,
                beforeShowDay:  function(dt)
        	      {
        	        return [dt.getDay() == 0, ""];
        	      },
                      
                onSelect: function (dateText, inst) {
                 
                 
                 $('#AllocatedDate').datepicker("option", 'maxDate', dateText);
                  
                },      
    
                onClose: function(dateText, inst) { 
                
                 setEngineerValue();
                
                 //To do: get the data from selected week. 
                 fillFormData($("#ServiceProviderEngineerIDHidden").val(), $("#AllocatedDate").val(), $("#AllocatedToDate").val(), $("#AppointmentAllocationSlotID").val(), $("#CurrentWeekNumber").val());
                 
                 formElemnts();
                    
                }
            });       
    
    
     function fillFormData($ServiceProviderEngineerID, $AllocatedDate, $AllocatedToDate, $AppointmentAllocationSlotID, $CurrentWeekNumber)
     {
           
           $.post("{$_subdomain}/Data/getDiaryAllocation/",        

            { ServiceProviderEngineerID: $ServiceProviderEngineerID, AllocatedDate: $AllocatedDate, AllocatedToDate: $AllocatedToDate, AppointmentAllocationSlotID: $AppointmentAllocationSlotID },  
                
            function(data){
                if(data!='false')
                {    
                    var p = eval("(" + data + ")");

                    if($CurrentWeekNumber)
                    {
                            $("#CurrentWeekNumber").val($CurrentWeekNumber);
                    }    
                    else
                    {
                    
                        {if $PreSelectWeekNumber}

                            $("#CurrentWeekNumber").val("{$PreSelectWeekNumber}");

                        {else}

                    $("#CurrentWeekNumber").val('1');//Setting current week number to 1.
                    
                        {/if}

                    }
                    
                    var $hiddenMapPostcodeHTMLCode = '';
                    
                    var cnt = 0;
                   // for (k in p) cnt++;
                   
                    $.each(p, function(i,v) { cnt++; });
                    
                    //cnt = p.length;
                    //alert(cnt);

                    for($i=1;$i<=cnt;$i++)
                    {
                        
                       $hiddenMapPostcodeHTMLCode += '<input id="hiddenAllocatedSlots_'+$i+'" type="hidden" value="'+p[$i]['NumberOfAllocations']+'" name="hiddenAllocatedSlots_'+$i+'" >';
                       $hiddenMapPostcodeHTMLCode += '<input id="hiddenPostcode_'+$i+'" type="hidden" value="'+p[$i]['PostCode']+'" name="hiddenPostcode_'+$i+'" >';
                       $hiddenMapPostcodeHTMLCode += '<input id="hiddenDisplayDay_'+$i+'" type="hidden" value="'+p[$i]['DisplayDay']+'" name="hiddenDisplayDay_'+$i+'" >';
                       $hiddenMapPostcodeHTMLCode += '<input id="hiddenDisplayDate_'+$i+'" type="hidden" value="'+p[$i]['Date']+'" name="hiddenDisplayDate_'+$i+'" >';
                       $hiddenMapPostcodeHTMLCode += '<input id="hiddenDayStatus_'+$i+'" type="hidden" value="'+p[$i]['DayStatus']+'" name="hiddenDayStatus_'+$i+'" >';
                       $('#AllocatedSlots_'+$i).val(p[$i]['NumberOfAllocations']);
                    }
                    
                    $CurrentWeekNumber = parseInt($("#CurrentWeekNumber").val());
                    
                    $startDayNo  = (($CurrentWeekNumber-1)*7); 

                    for($i=1;$i<=7;$i++)
                    {
                       $("#AllocatedSlots_"+$i).val(p[($startDayNo+$i)]['NumberOfAllocations']);
                       $("#Postcode_"+$i).val(p[($startDayNo+$i)]['PostCode']);
                    
                    }
                    
                    $("#hiddenPostCodeMaps").html($hiddenMapPostcodeHTMLCode);
                    $("#TotalWeeks").html(cnt/7);
                    $("#TotalWeeksValue").val(cnt/7);
                    
                    $("#WeeksDisplyedText").html("{$page['Labels']['weeks_displyed']|escape:'html'} "+p[1]['StartWeekDay']+" {$page['Labels']['to_text']|escape:'html'} "+p[1]['EndWeekDay']+"<br><br>").css("text-align", "center");
                    
                    
                    $("#CurrentWeekNumber").trigger("change");
                    
                    if((cnt/7)>1)
                    {
                        $("#WeekNavigationElement").show();
                        
                        $("#ReplicateWeeksElement").show();
                        
                    }
                    else
                    {
                        $("#WeekNavigationElement").hide();
                        
                        $("#ReplicateWeeksElement").hide();
                    }
                    
                    
                    
                    
                    
                    
                    $('#secondDiv').css("height", '');
                    adjustPanelHeights();
                    
                }
                else
                {
                    for($i=1;$i<=7;$i++)
                    {
                       $("#AllocatedSlots_"+$i).val('0');
                       $("#Postcode_"+$i).val('');
                    }
                    $("#hiddenPostCodeMaps").html('');
                    $("#CurrentWeekNumber").val('1');//Setting current week number to 1.
                    $("#TotalWeeksValue").val('1');
                    $("#TotalWeeks").html('1');
                    
                    $("#CurrentWeekNumber").trigger("change");
                    
                    $("#WeeksDisplyedText").html('');
                    
                    $("#WeekNavigationElement").hide();
                     
                   // $("#ReplicateWeeks").trigger("click");
                     
                    $('#secondDiv').css("height", '');
                    adjustPanelHeights();
                    
                    
                }
                
                
                
            }); //Post ends here...
     }
     
     
     
     
     
        //Click handler for Week buttons.
                $(document).on('click', '#WeekButtons label', 
                      function() {
                      
                         $success = assignDataToHiddenVariables();
                                
                         if($success)
                         {
                                 $('#WeekButtons label').each(function() {
                
                                    $(this).removeClass("weekButtonsActive"); 
                                    $(this).addClass("weekButtons");

                                 }); 
                                 
                                $showID = $(this).attr("id").replace("weekButtons", "");

                                $("#CurrentWeekNumber").val($showID);
                                $("#CurrentWeekNumber").trigger("change");

                                $(this).addClass("weekButtonsActive");
                         }
                      
                      }); 
     
     
     
     
     
    //Click handler for previous week button.
    $(document).on('click', '#prevWeek', 
                            function() {
                            
                                $success = assignDataToHiddenVariables();
                                
                                if($success)
                                {
                                    $CurrentWeekNumber = parseInt($("#CurrentWeekNumber").val());
                                    $("#CurrentWeekNumber").val($CurrentWeekNumber-1);
                                    $("#CurrentWeekNumber").trigger("change");
                                }
                            
                            });
                            
                            
    //Click handler for next week button.
    $(document).on('click', '#nextWeek', 
                            function() {
                            
                                $success = assignDataToHiddenVariables();
                                
                                if($success)
                                {
                                    $CurrentWeekNumber = parseInt($("#CurrentWeekNumber").val());
                                    $("#CurrentWeekNumber").val($CurrentWeekNumber+1);
                                    $("#CurrentWeekNumber").trigger("change");
                                }
                            
                            });
     
    
    //This function assign data to hidden vairables
    function assignDataToHiddenVariables()
    {
       $CurrentWeekNumber = parseInt($("#CurrentWeekNumber").val());
                    
       $startDayNo  = (($CurrentWeekNumber-1)*7); 

       for($i=1;$i<=7;$i++)
       {
          $("#hiddenAllocatedSlots_"+($startDayNo+$i)).val($("#AllocatedSlots_"+$i).val());
          $("#hiddenPostcode_"+($startDayNo+$i)).val($("#Postcode_"+$i).val());
       }
       
       return true;
    }
    
    //Change handler for current week number number.
    $(document).on('change', '#CurrentWeekNumber', 
                            function() {
                            
                          
                          
                            $("#prevWeek").show();
                            $("#nextWeek").show();
                            
                            if($("#CurrentWeekNumber").val()=="1")
                            {
                                $("#prevWeek").hide();
                            }
                            if($("#CurrentWeekNumber").val()==$("#TotalWeeksValue").val())
                            {
                                $("#nextWeek").hide();
                            }
                            
                            if($("#CurrentWeekNumber").val()=="")
                            {
                                $("#nextWeek").hide();
                                $("#prevWeek").hide();
                            }
                            else {
                            
                                 $CurrentWeekNumber = parseInt($("#CurrentWeekNumber").val());
                    
                                 $startDayNo  = (($CurrentWeekNumber-1)*7); 
                                 
                                 
                                 $(".DateSaveButton").css("text-decoration", "underline");
                               $( "#tabs" ).trigger("select");
                                 for($i=1;$i<=7;$i++)
                                 {
                                        $("#AllocatedSlots_"+$i).val($("#hiddenAllocatedSlots_"+($startDayNo+$i)).val());
                                        $("#Postcode_"+$i).val($("#hiddenPostcode_"+($startDayNo+$i)).val());
                                        $("#PostcodeLabel_"+$i).html($("#hiddenDisplayDay_"+($startDayNo+$i)).val());
                                        $("#PostcodeLabel_"+$i).attr("href", "#"+$("#hiddenDisplayDate_"+($startDayNo+$i)).val());
                                  
                                        if($("#hiddenDayStatus_"+($startDayNo+$i)).val()=="Active")
                                        {    
                                            $("#DayStatus_"+$i).attr("src", "{$_subdomain}/css/Skins/{$_theme}/images/green_tick.png");
                                        }
                                        else
                                        {
                                            $("#DayStatus_"+$i).attr("src", "{$_subdomain}/css/Skins/{$_theme}/images/red_cross.png");
                                        }
                                        if($("#hiddenMapStatus_"+($startDayNo+$i)).val()=="Yes")
                                        {    
                                            $("#MapStatus_"+$i).attr("src", "{$_subdomain}/css/Skins/{$_theme}/images/green_tick.png");
                                        }
                                        else
                                        {
                                            $("#MapStatus_"+$i).attr("src", "{$_subdomain}/css/Skins/{$_theme}/images/red_cross.png");
                                        }
                                 }
                            
                            }
                            
                            
                            
                            
                            
                            });
                                       
    
    
    
       $(document).on('click', '#saveRecord', 
                            function() {
             
             
             $(window).queue(function() {
             
             $dateVal = $("#FocusDate").val();
             
            // alert($dateVal);
             
              skillsAreaMapFormValidate($dateVal);  
              
              $(this).dequeue();
                                                                    
             });//set delay ends here...      
                                                    
             $("#FocusDate").val('');
                            
        });    
        
        
        
        function skillsAreaMapFormValidate($FocusDate)
        {
                                     
                                 $("#FocusDateHidden").val($FocusDate);    
                                     
                                      
                                     
                                 setEngineerValue();
                                 assignDataToHiddenVariables();
                                 
                                 
                                 $('#skillsAreaMapForm').validate({
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                                    
                                                    ServiceProviderEngineerIDHidden:
                                                       {
                                                           required: true
                                                       },
                                                       
                                                    AppointmentAllocationSlotID:
                                                       {
                                                           required: true
                                                       },
                                                    
                                                    AllocatedDateHidden:
                                                       {
                                                           required: function (element) {
                                                                if($("#AllocatedDateHidden").val()=='' || $("#AllocatedToDateHidden").val()=='')
                                                                {
                                                                    
                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    return false;
                                                                }
                                                            }
                                                       }    
                                                       
                                                       
                                                   },
                                            messages: {
                                                      
                                                      ServiceProviderEngineerIDHidden:
                                                       {
                                                           required: "{$page['Errors']['engineer']|escape:'html'}"
                                                       },
                                                       
                                                       AppointmentAllocationSlotID:
                                                       {
                                                           required: "{$page['Errors']['allocation_slot']|escape:'html'}"
                                                       },
                                                       
                                                       AllocatedDateHidden:
                                                       {
                                                           required: "{$page['Errors']['allocated_date_range']|escape:'html'}"
                                                       }
                                                    },
                                           
                                           errorPlacement: function(error, element) {
                                               // alert("sssss");
                                                error.insertAfter( element );
                                                
                                                $('#secondDiv').css("height", '');
                                                adjustPanelHeights();
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                                
                                                $("#saveRecord").hide();
                                                $("#cancelChanges").hide();
                                                $("#processDisplayText").show();
                                                
                                                
                                                $.colorbox({ 
                                                
                                                            html:$('#waitDiv').html(), 
                                                            title:"",
                                                            escKey: false,
                                                            overlayClose: false,
                                                            onLoad: function() {
                                                                $('#cboxClose').remove();
                                                            }
                                                
                                                    });
                                                
                                                
                                                // alert("dddd"+$("#FocusDateHidden").val());
                                                
                                                
                                                $.post("{$_subdomain}/AppointmentDiary/ProcessData/ServiceProviderSkillsSet/",        

                                                        $("#skillsAreaMapForm").serialize(),      
                                                        function(data){
                                                            
                                                            var p = eval("(" + data + ")");

                                                                    if(p['status']=="ERROR")
                                                                    {
                                                                        
                                                                        $("#errorCenterInfoText").html(p['message']).css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  
                                                                        
                                                                       $.colorbox.close();

                                                                    }
                                                                    else if(p['status']=="SLOT_ERROR")
                                                                    {
                                                                        fillFormData($("#ServiceProviderEngineerIDHidden").val(), $("#AllocatedDate").val(), $("#AllocatedToDate").val(), $("#AppointmentAllocationSlotID").val(), $("#CurrentWeekNumber").val());
                                                                        
                                                                        $("#ReplicateWeeks").attr('checked', false);
                                                                        $("#ReplicateToOtherSlot").attr('checked', false);
                                                                        
                                                                        $("#errorCenterInfoText").html("{$page['Errors']['slots_error1']|escape:'html'}<br>{$page['Errors']['slots_error2']|escape:'html'}").css('color','red').fadeIn('slow').css('text-align','left');  
                                                                        
                                                                       $.colorbox.close();

                                                                    }
                                                                    else if(p['status']=="OK")
                                                                    {
                                                                       $("#ReplicateWeeks").attr('checked', false);
                                                                       $("#ReplicateToOtherSlot").attr('checked', false);
                                                                    
                                                                       fillFormData($("#ServiceProviderEngineerIDHidden").val(), $("#AllocatedDate").val(), $("#AllocatedToDate").val(), $("#AppointmentAllocationSlotID").val(), $("#CurrentWeekNumber").val());

                                                                       $("#centerInfoText").css('color','').html("{$page['Text']['data_saveed_msg']|escape:'html'}").fadeIn('slow').delay("5000").fadeOut('slow');  
                                                                      
                                                                      // alert($("#FocusDateHidden").val());
                                                                      
                                                                       
                                                                      
                                                                       if($("#FocusDateHidden").val())
                                                                       {
                                                                          document.location.href = "{$_subdomain}/AppointmentDiary/index/update/"+$("#ServiceProviderEngineerIDHidden").val()+"/d="+$("#FocusDateHidden").val();
                                                                       }
                                                                       else
                                                                       {    
                                                                            $.colorbox.close();
                                                                            
                                                                            {*$('#WeekButtons label').each(function() {

                                                                                $(this).removeClass("weekButtonsActive"); 
                                                                                $(this).addClass("weekButtons");

                                                                             });
                                                                             
                                                                            $('#weekButtons1').addClass("weekButtonsActive");*}
                                                                       }   
                                                                     // document.location.href = "{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val()+"/tInfo=1";
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    
                                                                    $("#processDisplayText").hide();
                                                                    $("#saveRecord").show();
                                                                    $("#cancelChanges").show();
                                                                    

                                                                    $('#secondDiv').css("height", '');
                                                                    adjustPanelHeights();
                                                                    
                                                                    
                                                        }); //Post ends here...
                                                
                                                
                                                
                                            }
                                                    
                            
                            
                            });
    
             }
             
             
             
        //Service Proivder Skill Set data table starts here...   
        $('#SPSSResults').PCCSDataTable( {


                "aoColumns": [ 
                                            /* ServiceProviderEngineerID */  {  "bVisible":    false },    
                                            /* Engineer Name */   {  "bSortable":    false },
                                            /* Status */ {  "bSortable":    false }
                                    ],






                displayButtons:  '',    
                htmlTablePageId: 'innerFirstDiv',
                htmlTableId:     'SPSSResults',
                fetchDataUrl:    '{$_subdomain}/AppointmentDiary/ProcessData/Engineers/fetch/{$ServiceProviderID}/{$ShowAll}',
                fnRowCallback:   'inactiveRow',
                searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                iDisplayLength:  19,
                sDom: 't<"#dataTables_command">rp',
                "aaSorting": [[ 1, "asc" ]],
                sPaginationType: 'two_button',
                hidePaginationNorows:  true,
                bServerSide: false,
                formInsertButton:'insert_save_btn',
                
                            frmErrorRules:   {
                                            
                                                       
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                 
                                                       
                                             }
                            



            });
         //Service Proivder Skill Set data table ends here...       
    
    
    
    $(document).on('click', '.DateSaveButton', 
                            function() {
                            
                              
                               $FocusDate = $(this).attr("href").replace("#","");
                               
                               $("#FocusDate").val($FocusDate);
                               $("#saveRecord").trigger("click");
                               
                               return false;
                                
                            });
                            
                            
    $(document).on('click', '#EngineerDefaultsSubMenuButton', 
                            function() {
                            
                              
                               $FocusDate = $("#PostcodeLabel_1").attr("href").replace("#","");
                               
                               
                               $(window).queue(function() {
                               
                                  $("#FocusDate").val($FocusDate);
                               
                                  $(this).dequeue();
                               });
                               
                               $("#saveRecord").trigger("click");
                               
                               return false;
                                
                            });                        
    
    
   
    $("#CurrentWeekNumber").trigger("change");
    
    {if $datarow.AllocatedDate neq '' && $datarow.AllocatedToDate neq '' && $datarow.ServiceProviderEngineerID neq ''}

            
           
            
           // alert($("#ServiceProviderEngineerIDHidden").val());
           
            setEngineerValue();
               
            //To do: get the data from selected week. 
            fillFormData($("#ServiceProviderEngineerIDHidden").val(), $("#AllocatedDate").val(), $("#AllocatedToDate").val(), $("#AppointmentAllocationSlotID").val(), $("#CurrentWeekNumber").val());

            formElemnts();  
            
            $('#AllocatedToDate').datepicker("option", 'minDate', $("#AllocatedDate").val());
            $('#AllocatedDate').datepicker("option", 'maxDate', $("#AllocatedToDate").val());
            
            $("#Postcode_{$datarow.FocusDayNo}").focus();
          
            
    {/if}   
        
         
        
    
    
    });
    
   function editMap(n){
   date=$('#PostcodeLabel_'+n).attr('href').replace("#","");
   engineer=$('#ServiceProviderEngineerIDHidden').val();
   start=$('#PostcodeLabel_'+1).attr('href').replace("#","");
   window.location="{$_subdomain}/Diary/loadGridMap/d="+date+"/e="+engineer+"/s="+start+"/sp="+$("#ServiceProviderID").val();
   }
   
   function copyMapData(n)
   {
   date=$('#PostcodeLabel_'+n).attr('href').replace("#","");
   engineer=$('#ServiceProviderEngineerIDHidden').val();
   start=$('#PostcodeLabel_'+1).attr('href').replace("#","");
   $.post("{$_subdomain}/Diary/copyGeoMapToSession",{ 'd':date,'e':engineer,'s':start },
function(data) {
    $('.geoCopyBut').hide();
    $('.geoPasteBut').show();
    $('#copyTextID').hide();
    $('#pasteTextID').show();
    
  
    });
   }
   
   
   function pasteMapData(n)
   {
       date=$('#PostcodeLabel_'+n).attr('href').replace("#","");
   engineer=$('#ServiceProviderEngineerIDHidden').val();
   start=$('#PostcodeLabel_'+1).attr('href').replace("#","");
    $.post("{$_subdomain}/Diary/pasteGeoMapToDatabase",{ 'd':date,'e':engineer,'s':start },
function(data) {
   // $('.geoCopyBut').show();
    //$('.geoPasteBut').hide();
    $('#MapStatus_'+n).attr('src',"{$_subdomain}/css/Skins/{$_theme}/images/green_tick.png");
    nn=n*1+($('#CurrentWeekNumber').val()*7-7);
    $('#hiddenMapStatus_'+nn).val('Yes');
 
    });
   }
</script>


    
{/block}

{block name=body}

    
<div class="breadcrumb">
    
    
    
    <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> / 
        <a href="{$_subdomain}/index/siteMap" >{$page['Text']['site_map']|escape:'html'}</a>  / 
        <a href="{$_subdomain}/AppointmentDiary" >{$page['Text']['appointment_diary']|escape:'html'}</a> / 
        {$page['Text']['page_title']|escape:'html'}
    </div>
</div>
 
{include file='include/site_map_menu.tpl'}

        
<div class="main" id="appointmentDiary" >   
   
   <div class="siteMapPanel" >
     
         <form id="skillsAreaMapForm" name="skillsAreaMapForm" method="post"  action="#" class="inline">
       
             
            <fieldset>
            
             
                        <div id="firstDiv" class="firstDiv borderDiv">
                           
                           <div id="innerFirstDiv" >
                            
                            {if $SuperAdmin}
                            
                             <select  name="ServiceProviderID" id="ServiceProviderID"  class="text auto-hint" >
                                {foreach from=$serviceProvidersList item=sp}
                                 <option value="{$sp.ServiceProviderID}" {if $ServiceProviderID eq $sp.ServiceProviderID}selected="selected"{/if} >{$sp.CompanyName|escape:'html'}</option>
                                 {/foreach}

                             </select>  
                                 
                            {else}
                            
                                <input type="hidden" name="ServiceProviderID" id="ServiceProviderID" value="{$ServiceProviderID|escape:'html'}" >
                                
                             {/if}
                                 
                            
                            
                             
                             <table id="SPSSResults" border="0" cellpadding="0" cellspacing="0" class="browse" style="padding-top:4px;" >
                                <thead>
                                        <tr>
                                                <th></th> 
                                                <th width="90%" title="{$page['Text']['engineers']|escape:'html'}"  >{$page['Text']['engineers']|escape:'html'}</th>
                                                <th></th>
                                        </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table> 
                             
                             <input type="hidden" name="ServiceProviderEngineerIDHidden" id="ServiceProviderEngineerIDHidden" value="" > 
                             
                             
                             
                                 
                           </div>
                        </div>
                        
                        <div id="secondDiv" class="secondDiv borderDiv" > 
                            
                            <p style="text-align:center;" >
                                <label class="subMenuButtons" style="width:265px;padding:10px 0px 10px 0px;margin:0px;font-size: 14px;" id="EngineerDefaultsSubMenuButton" >{$page['Labels']['engineer_defaults']|escape:'html'}</label>
                                
                                {if $dat=="GridMapping"}
                                <label  class="subMenuButtonsActive" style="width:265px;padding:10px 0px 10px 0px;margin:0px;font-size: 14px;" id="GeoCellAllocationSubMenuButton" >Map Area Allocation</label>
                               {else}
                                    <label class="subMenuButtonsActive" style="width:265px;padding:10px 0px 10px 0px;margin:0px;font-size: 14px;" id="PostcodeAllocationSubMenuButton" >{$page['Labels']['postcode_allocation']|escape:'html'}</label>
                                   {/if}
                               
                            </p>
                            
                            <div id="centerInfoText" style="display:none;padding-top:0px;" class="centerInfoText" ></div>
                            <div id="errorCenterInfoText" style="display:none;padding-top:0px;" class="centerInfoText" ></div>
                            
                            <p>&nbsp;</p>
                            
                             <p>
                                
                                
                                <span id="DateSelectionPanel" style="padding-left:0px;margin-left:0px;float:left;" >
                                
                                {$page['Labels']['week_commencing_from']|escape:'html'}: <label style="padding-right:0px;" ><input  type="text" name="AllocatedDate" class="text" style="width:80px;" id="AllocatedDate" value="{$datarow.AllocatedDate|escape:'html'}" readonly="readonly" ></label>
                                {$page['Labels']['to_date']|escape:'html'}&nbsp;&nbsp;<label><input  type="text" name="AllocatedToDate" class="text" style="width:80px;" id="AllocatedToDate" value="{$datarow.AllocatedToDate|escape:'html'}" readonly="readonly" ></label>
                                
                                
                                <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EngPostCodeMapDateHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                
                                </span>
                                
                                
                                <span style="margin-left:200px;float:left;" >
                                <input type="hidden" name="AllocatedDateHidden" id="AllocatedDateHidden"   value="" > 
                                <input type="hidden" name="AllocatedToDateHidden" id="AllocatedToDateHidden"   value="" > 
                                </span>
                            </p>  
                            
                            <p style="text-align:center;padding-bottom:5px;" >
                            
                                <span id="ReplicateToOtherSlotElement" style="display:none">
                                {if $SetupUniqueTimeSlotPostcodes eq "Yes"}
                                    
                                     <input  type="checkbox" name="ReplicateToOtherSlot" id="ReplicateToOtherSlot" value="1" > {$page['Labels']['replicate_to_other_slot']|escape:'html'}
                                    
                                    
                                {else}
                                    
                                    <input  type="hidden" name="ReplicateToOtherSlot" id="ReplicateToOtherSlot" value="1" >
                                    
                                {/if}
                                </span>
                                &nbsp;&nbsp;
                                <span id="ReplicateWeeksElement_disabled" style="display:none" >
                                 <input  type="checkbox" name="ReplicateWeeks" id="ReplicateWeeks" value="1" > {$page['Labels']['replicate_all_weeks']|escape:'html'}
                                </span>
                            </p>
                            
                            
                            
                            
                            <p style="text-align:center;" >
                                
                            <table style="width:400px;display:none;margin-left:30px;" id="WeekNavigationElement"  >
                                <tr>
                                    <td style="text-align:right" >{$page['Labels']['displayed_week']|escape:'html'}:</td>
                                    <td width="19" style="text-align:center" ><img id="prevWeek" src="{$_subdomain}/css/Skins/{$_theme}/images/back_enabled_hover.png" width="19" height="19"  title="{$page['Text']['prev']|escape:'html'}" alt="{$page['Text']['prev']|escape:'html'}"  style="cursor:pointer;" ></td>
                                    <td width="20" style="text-align:center" ><input type="text" name="CurrentWeekNumber" id="CurrentWeekNumber" readonly="readonly" style="width:20px;"  value="" ></td>
                                    <td width="19" style="text-align:center" ><img id="nextWeek" src="{$_subdomain}/css/Skins/{$_theme}/images/forward_enabled_hover.png" width="19" height="19" style="cursor:pointer" title="{$page['Text']['next']|escape:'html'}" alt="{$page['Text']['next']|escape:'html'}" ></td>
                                    <td width="40" style="text-align:left" id="TotalWeeks" >
                                    </td>
                                </tr>
                            </table>   
                              <input name="TotalWeeksValue" id="TotalWeeksValue"  type="hidden" value="" >
                            
                            </p>
                            
                            
                            
                            
                            
                            
                            <table id="PostCodeMapTable" style="width:300px;float:left">
                                
                                <tr>
                                    <td class="topFirstCol" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EngPostCodeMapHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" ></td>
                                    <td class="topSecondCol" >
                                            <div id="tabs" style="border-style:none;" >
                                                    <ul style="background-color:transparent;background-image: url('');border-style:none;" >
                                                           
                                                           
                                                           {foreach from=$AppointmentAllocationSlots item=aas}
                                                               
                                                            <li id="Tab{$aas.AppointmentAllocationSlotID}" ><a href="#tabs-{$aas.AppointmentAllocationSlotID}"  >{$aas.Description|escape:'html'}</a></li>
                                                            
                                                           {/foreach}     
                                                    </ul>
                                                    {foreach from=$AppointmentAllocationSlots item=aas}
                                                               
                                                            <div id="tabs-{$aas.AppointmentAllocationSlotID}" style="padding:0px;margin:0px;height:0px;"></div>
                                                            
                                                    {/foreach}  
                                                    
                                                   

                                            </div>
                                        
                                        <input type="hidden" name="AppointmentAllocationSlotID" id="AppointmentAllocationSlotID" value="{$datarow.AppointmentAllocationSlotID|escape:'html'}" >
                                        
                                        
                                     </td>
                                </tr>
                                
                                {for $r=1 to 7}
                                <tr>
                                    
                                      <td class="firstCol" style="vertical-align:middle;" ><a href="#" class="DateSaveButton" id="PostcodeLabel_{$r}" style="padding:0px;margin:0px;" ></a>
                                          <br><img src="{$_subdomain}/css/Skins/{$_theme}/images/red_cross.png" id="DayStatus_{$r}" align="center" style="padding-top:5px;"  width="20" height="20" >
                                          <input  {if $def!=1} type="text"{else}type="hidden" {/if} style="width:20px"  name="AllocatedSlots_{$r}" id="AllocatedSlots_{$r}"  class="text" value="" >
                                      </td>
                                      <td class="secondCol" >
                                          <button class="btnStandard " id="editmap_{$r}" onclick="editMap('{$r}');"  type="button" style="width:65px">Edit</button>
                                          <button id="geoCopyBut" class="btnStandard geoCopyBut" type="button" onclick="copyMapData('{$r}')" style="width:65px">Copy</button>
                                          <button id="geoPasteBut" style="display:none" class="btnStandard geoPasteBut" type="button" onclick="pasteMapData('{$r}')" style="width:65px">Paste</button>
                                         <img title="Map allocation status" src="{$_subdomain}/css/Skins/{$_theme}/images/red_cross.png" id="MapStatus_{$r}" align="center" style=";margin-top: 6px;padding-top:5px;"  width="20" height="20" >
                                      </td>
                                </tr> 
                                
                                {/for}
                                
                                
                                
                                <tr>
                                    <td id="hiddenPostCodeMaps" >
                                                                       
                                    </td>
                                </tr>
                                
                                
                             </table>
                                        
                                        
                                <div id="copyTextID" style="padding:15px;position:relative;float:right;margin-top: 42px;border:1px solid #CCCCCC;width:220px;height:333px;text-align: center" >Select the "Copy Map" button to replicate the Map Area Allocations to a alternative day or alternative engineer.</div>
                                <div id="pasteTextID" style="padding:15px;position:relative;float:right;margin-top: 42px;border:1px solid #CCCCCC;width:220px;height:333px;text-align: center;display:none;" >Select an alternative day or an alternative engineeer and click the Paste button.<button class="gplus-red" style="position:relative;float:right;top:200px;margin-top:20px">Cancel</button></div>
                            
                            <p>
                                <span id="WeeksDisplyedText" style="padding-left:0px;margin-left:0px;float:left;" >
                              
                                </span>
                            
                            </p>
                            
                        </div>
                       
                       <div id="thirdDiv" class="firstDiv assistDiv" >
                        
                       </div>
                       
                       
                       <div id="fourthDiv" class="secondDiv assistDiv"  >  
                       
                           <input type="hidden" name="SkillsAreaMap" id="SkillsAreaMap"   value="1" > 
                           
                           <input  type="hidden" name="FocusDate" id="FocusDate"  value="" />
                           
                           <input  type="hidden" name="FocusDateHidden" id="FocusDateHidden"  value="" />
                           
                           
                           
                           <input  type="submit" name="saveRecord" id="saveRecord"    class="form-button"   value="{$page['Buttons']['save_record']|escape:'html'}" > 
                           &nbsp;&nbsp;
                           <input style="display:none" type="submit" name="cancelChanges" id="cancelChanges"  class="form-button"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 
                           
                           
                           <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>    
                           <span id="cancelChangesMsg" ></span>
                           
                       </div>
                   </fieldset> 

        
        </form>
    </div>  
                                    
</div>
                           
<div id="waitDiv" style="display:none">
    <img src="{$_subdomain}/images/processing.gif"><br>
</div>

         
{/block}
