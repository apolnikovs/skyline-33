{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Product Details"}
{$PageId = $ProductDetailsPage}
{/block}


{block name=scripts}

    <script>


	$(document).on('click', ".spaceBlock", function() {

	    $("#prefilForm").attr("action",$(this).attr("href"));
	    
	    $(".productLabel").each(function() {
		switch ($(this).text()) {
		    case "Item Catalogue No:": {
			$("#jbStockCodePrefill").val($(this).next().text());
			break;
		    }
		    case "Product type:": {
			$("#jbUnitTypeIDPrefill").val($(this).next().text());
			break;
		    }
		    case "Manufacturer:": {
			$("#jbManufacturerIDPrefill").val($(this).next().text());
			break;
		    }
		    case "Model No:": {
			$("#jbModelNamePrefill").val($(this).next().text());
			break;
		    }
		}
	    });
	    
	    $("#prefilForm").submit();
	    
	    return false;
	});


    </script>

{/block}


{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/productsearch">Product Search</a> / Product Details
    </div>
</div>
        
<div class="main" id="productDetails">
        
    <div class="productDetailsPanel">
        <h2>Product Details</h2>
        <p>
            <span class="productLabel">Item Catalogue No:</span>
            <span class="productValue">{$Product['ProductNo']}</span>
        </p>
        <p>
            <span class="productLabel">Item Description:</span>
            <span class="productValue">{$Product['ModelDescription']}</span>
        </p>
        <p>
            <span class="productLabel">Product type:</span>
            <span class="productValue">{$Product['UnitTypeName']}</span>
        </p>
        <p>
            <span class="productLabel">Manufacturer:</span>
            <span class="productValue">{$Product['ManufacturerName']}</span>
        </p>
        <p>
            <span class="productLabel">Model No:</span>
            <span class="productValue">{$Product['ModelNumber']}</span>
        </p>
    </div>
        
    <div class="serviceInstructionsBar"></div>
    <div class="serviceInstructionsPanel">
        <hr />
        <h2>Service Instructions</h2>
        <h3>Help Line: 01234 654784</h3>
        <p>
            This unit can be repaired in the customer's home.
        </p>
        <h4>IMPORTANT NOTE</h4>
        <p>
            FOR STORES USE ONLY: there is no need to telephone before sending 
            the faulty product to Camtronics.  This item is repaired by 
            Camtronics. telephone 01462 485947
        </p>
        <h3>Spares Information</h3>
        <p>
            Chargeable spares are avaiable to prchase.
        </p>
        <p>
            Spares Order No: 123456789
        </p>
        <hr />
    </div>
   
    {if isset($loggedin_user->ClientID)}
        
    <div class="serviceActionsPanel">
        <h2>Service Actions</h2>
        <p>
            The options available for this product are displayed below. 
            Please select the option that best suits your customer's requirements.
        </p>
        
{*        <div class="row">
            <div class="prepend-1 span-2 append-1"><a href="/Job" ><div class="textButton">Book<br />Repair</div></a></div>
            <div class="span-7 textEnabled">You may book a repair for this product</div> 
        
            <div class="prepend-1 span-2 append-1"><div class="textButtonDisabled">Order<br />Parts</div></div>
            <div class="span-7 last textDisabled">You may order spares for this product</div> 
        </div>
        
        <div class="row">&nbsp;</div>
        
         <div class="row">
            <div class="prepend-1 span-2 append-1"><a href="/Job" ><div class="textButton">Book<br />Install</div></a></div>
            <div class="span-7 textEnabled">You may book an installation for this product</div> 
        
            <div class="prepend-1 span-2 append-1"><div class="textButtonDisabled">Tech<br />Info</div></div>
            <div class="span-7 last textDisabled">You mayselect this option to look up technical 
                    information or link to a manufacturer's website.</div> 
        </div>*}

	
	
	
	<div style="clear:both;">
	
	    <form id="newJobBookingForm" name="newJobBookingForm" method="get"  >

		<fieldset style="padding-right: 40px;padding-top: 18px;padding-bottom:18px">

		    <legend>New Job Booking</legend>

		    <p>Select one of these options to commence booking a new service job</p>

		    <p style="margin-bottom:0;">
			<span style="float:left;" >
			    {if isset($loggedin_user->ClientID)}
				{foreach $JobTypesList as $jtype}     
				    <a  href="{$_subdomain}/Job/index/JobTypeID={$jtype.JobTypeID}" class="spaceBlock" >{$jtype.Name|escape:'html'}</a>
				{/foreach} 
			    {else}
				 {foreach $JobTypesList as $jtype}     
				    <a  href="#" id="JobTypeID_{$jtype.JobTypeID|escape:'html'}" class="spaceBlock PopUp" >{$jtype.Name|escape:'html'}</a>
				 {/foreach} 
			    {/if}    
			</span>
		    </p>

		</fieldset>

	    </form>
			
			
	    <form id="prefilForm" method="post" action="">
		<input type="hidden" id="jbStockCodePrefill" name="jbStockCodePrefill" value="" />
		<input type="hidden" id="jbUnitTypeIDPrefill" name="jbUnitTypeIDPrefill" value="" />
		<input type="hidden" id="jbManufacturerIDPrefill" name="jbManufacturerIDPrefill" value="" />
		<input type="hidden" id="jbModelNamePrefill" name="jbModelNamePrefill" value="" />
	    </form>	

			
	</div>
                        
		
			

			    
    </div>
                        
    {/if}	                    
                                    
</div>
{/block}